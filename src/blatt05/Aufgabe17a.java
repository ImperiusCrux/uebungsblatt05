package blatt05;

import java.awt.*;
import javax.swing.*;

public class Aufgabe17a extends JFrame {
	
	JLabel label;
	JTextField text;
	JButton butt1;
	JButton butt2;
	
	public Aufgabe17a() {
		super ("Aufgabe17a");
		setVisible(true);
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout());
		panel1.setBackground(Color.GRAY);
		this.add(panel1, BorderLayout.NORTH);
		
		label = new JLabel("Name: ");
		label.setFont(new Font("Arial", Font.PLAIN, 20));
		
		text = new JTextField();
		text.setEditable(true);
		text.setOpaque(true);
		text.setFont(new Font("Arial", Font.PLAIN , 20));
		panel1.add(label);
		panel1.add(text);
		this.add(panel1, BorderLayout.NORTH);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout());
		this.add(panel2, BorderLayout.SOUTH);
		
		butt1 = new JButton("OK");
		butt1.setFont(new Font("Arial", Font.BOLD, 20));
		panel2.add(butt1);
		
		butt2 = new JButton("Cancel");
		butt2.setFont(new Font("Arial", Font.BOLD, 20));
		panel2.add(butt2);
		this.add(panel2, BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
	

	}

	public static void main(String[] args) {
		new Aufgabe17a();

	}

}
