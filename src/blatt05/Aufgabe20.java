package blatt05;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

public class Aufgabe20 extends JFrame implements ActionListener{
	
	JButton btn1;
	JButton btn2;
	ArrayList<String> log;
	
	public Aufgabe20() {
		super("Aufgabe20");
		setSize(200, 200);
		setVisible(true);
//		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout());
		
		btn1 = new JButton("Close Application");
		btn1.addActionListener(this);
		panel.add(btn1);
		btn2 = new JButton("Show Log");
		btn2.addActionListener(this);
		panel.add(btn2);
		this.add(panel);
		addWindowListener(new WindowAdapter() {
			public void windowOpened(WindowEvent e) {
				log.add("Window opened at: " + System.currentTimeMillis());
			}
			public void windowActivated(WindowEvent e) {
				log.add("Window activated at: " + System.currentTimeMillis());
			}
			public void windowDeactivated(WindowEvent e) {
				log.add("Window deactivated at: " + System.currentTimeMillis());
			}
			public void windowGainedFocus(WindowEvent e) {
				log.add("Window focus lost at: " + System.currentTimeMillis());
			}
			public void windowLostFocus(WindowEvent e) {
				log.add("Window focus lost at: " + System.currentTimeMillis());
			}
			public void windowIconified(WindowEvent e) {
				log.add("Window iconified at: " + System.currentTimeMillis());
			}
			public void windowDeiconified(WindowEvent e) {
				log.add("Window de-iconified at: " + System.currentTimeMillis());
			}
			public void windowClosing(WindowEvent e) {
				log.add("Attempt to close the window at: " + System.currentTimeMillis());
			}
			public void windowClosed(WindowEvent e) {
				log.add("Attempt to close the window at: " + System.currentTimeMillis());
			}
			
		});
		
		
		
		
		
		pack();
		
		
	}
	
	
	
	
	
	
	


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
public static void main(String[] args) {
		new Aufgabe20();

	}

}
