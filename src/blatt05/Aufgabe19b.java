package blatt05;

import java.awt.event.*;

import javax.swing.*;

public class Aufgabe19b extends JFrame implements KeyListener{
	
	public Aufgabe19b() {
		super("Aufgabe19b");
		setVisible(true);
		setSize(500, 500);
		addKeyListener(this);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				JOptionPane.showMessageDialog(null, "Close application using the key 'x'");
				
			}
		});
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyChar() == 'x')
			dispose();
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}
	
	public static void main(String[] args) {
		new Aufgabe19b();

	}
}
