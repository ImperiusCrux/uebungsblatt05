package blatt05;

import javax.swing.*;
import java.awt.*;

public class Aufgabe17c extends JFrame{
	
	JTextField text;
	JButton butt;
	
	public Aufgabe17c() {
		super("Aufgabe17c");
		setVisible(true);
		setLocation(800, 420);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		JPanel north = new JPanel();
		north.setLayout(new GridLayout());
		
		text = new JTextField();
		text.setEditable(false);
		text.setSize(10, 20);
		north.add(text);
		this.add(north, BorderLayout.NORTH);
		
		JPanel south = new JPanel();
		south.setLayout(new GridLayout(5, 3));
		for(int i = 0; i < 10; i++) {
			butt = new JButton("" + i);
			south.add(butt);
		}
		
		String[] extras = {"+", "-", "=", ",", "C"};
		for(String counter : extras) {
			butt = new JButton("" + counter);
			south.add(butt);
		}
		this.add(south, BorderLayout.SOUTH);
		
		pack();
		
	}
	
	public static void main(String[] args) {
		new Aufgabe17c();
	}
}
