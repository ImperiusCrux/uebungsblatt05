package blatt05;

import java.awt.event.*;
import javax.swing.*;

public class Aufgabe19a extends JFrame implements ActionListener{
	
	private JButton bt1;
	
	public Aufgabe19a() {
		super("Aufgabe19a");
		setVisible(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		bt1 = new JButton("Change Title");
		bt1.addActionListener(this::actionPerformed);
		this.add(bt1);
		
		pack();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(bt1)) {
			this.setTitle(JOptionPane.showInputDialog("Neuen Titel eingeben:"));
		}
	}
	
	public static void main(String[] args) {
		new Aufgabe19a();

	}
}
