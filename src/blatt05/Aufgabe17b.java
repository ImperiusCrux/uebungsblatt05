package blatt05;

import javax.swing.*;
import java.awt.*;
import java.util.stream.Stream;

public class Aufgabe17b extends JFrame{
	
	JLabel label;
	JComboBox<String> selector;
	
	public Aufgabe17b() {
		super("Aufgabe17b");
		setLocation(600, 400);
		setVisible(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout());
		
		label = new JLabel("Alter: ");
		panel.add(label);
		
		selector = new JComboBox<String>();
		selector.addItem("none");
		Stream.iterate(0, i -> i + 1).limit(111).forEach(i -> selector.addItem(i.toString()));
		panel.add(selector);
		this.add(panel, BorderLayout.NORTH);
		
		pack();
	}
	
		
	public static void main(String[] args) {
		new Aufgabe17b();
	}
}
